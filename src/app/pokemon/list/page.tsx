
const PokemonCard = async(props: {name: string}) => {

    const response = await fetch("https://pokeapi.co/api/v2/pokemon/"+props.name)
    const data = await response.json()
    const imageUrl = data.sprites.other['official-artwork'].front_default

    return (
        <div>
            <img alt={props.name} src={imageUrl} />
            <div className="text-4xl text-center">{props.name}</div>
        </div>
    )
}

const PokemonList = async() => {
    const response = await fetch("https://pokeapi.co/api/v2/pokemon?limit=40&offset=0")
    const data = await response.json()

    return (
    <div className="grid grid-cols-4 gap-4 p-1">
        {
        data.results.map((pokemon: {name: string}) => (
            <PokemonCard key={pokemon.name} name={pokemon.name} />
        ))
        }
    </div>
    ) 
}

export default PokemonList